package simulator;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import algorithms.MazeExploration;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import simulator.Map;
import dataTypes.Orientation;
import simulatorRobot.RobotGenerator;
import simulatorRobot.Robot;


public class gui {
//    private Map map;
    private JPanel[][] mapGrids;
    private Robot bot;
    private RobotGenerator rg;
    JTextField exploreTextField, exploreTextField1, exploreTextField2;
	
    Color c=new Color(0f,0f,0f,0f );


    public gui(){
    	bot = new Robot();
    }

    public void display(){
        JFrame overallFrame = new JFrame("Map Arena Simulator");
        overallFrame.setPreferredSize(new Dimension(730, 700));
        JPanel mapFrame = new JPanel(new GridLayout(1, 1));
        mapFrame.setPreferredSize(new Dimension(500,500));
////        mapFrame.setLayout(null);
//        mapFrame.setLayout(new GridLayout(Arena.MAP_HEIGHT , Arena.MAP_LENGTH));
//        mapGrids = new JPanel[Arena.MAP_LENGTH][Arena.MAP_HEIGHT]; //allocate the size of grid
//        for(int j = Arena.MAP_HEIGHT - 1; j >= 0; j--) {
//            for(int i = 0; i < Arena.MAP_LENGTH ; i++) {
//                mapGrids[i][j]=new JPanel(); //creates new button
//                mapGrids[i][j].setPreferredSize(new Dimension(20, 20));
//                mapGrids[i][j].setEnabled(false);
//                mapGrids[i][j].setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY));
//                mapGrids[i][j].setBackground(c);
//                if ((i >= 0 && i <= 2 && j >= 0 && j <= 2) || (i >= 12 && i <= 14 && j >= 17 && j <= 19)) {
//                    mapGrids[i][j].setBackground(Color.RED);
////                    if (i == 13 && j == 18)
////                        mapGrids[i][j].setText("G");
////                    if (i == 1 && j == 1)
////                        mapGrids[i][j].setText("S");
//                    if (i == 2 && j == 1)
//                        mapGrids[i][j].setBackground(Color.BLUE);
//                }

//                mapFrame.add(mapGrids[i][j]); //adds button to grid
//            }
//        }
//        robotOrientation = Orientation.EAST;
//        mapFrame.setOpaque(false);
//		overallFrame.add(mapFrame, BorderLayout.WEST);
//		mapFrame.add(new RobotGenerator());
//		mapFrame.setBounds(0,115,500,55);
        rg = new RobotGenerator();
        mapFrame.add(rg);
        
        JPanel exploreAndPath = new JPanel(new GridLayout(2,1));
		
		JPanel exploreFrame = new JPanel();
		exploreFrame.setBorder(new EmptyBorder(20, 20, 20, 20));
		exploreFrame.setBorder(BorderFactory.createLineBorder(Color.RED));
		
		JPanel exploreInputFrame = new JPanel(new GridLayout(6, 2));
		exploreTextField = new JTextField("10");
		exploreTextField1 = new JTextField("100");
		exploreTextField2 = new JTextField("100");
		JLabel exploreLabel = new JLabel();
		exploreLabel.setText("Explore Arena");
		JLabel exploreEmpty = new JLabel();
		JLabel exploreLabel1 = new JLabel();
		exploreLabel1.setText("Intial Position: ");
		JLabel exploreLabel2 = new JLabel("2,1", SwingConstants.CENTER);
		JLabel exploreLabel3 = new JLabel();
		exploreLabel3.setText("Speed (steps/sec): ");
		JLabel exploreLabel4 = new JLabel();
		exploreLabel4.setText("Target Coverage (%): ");
		JLabel exploreLabel5 = new JLabel();
		exploreLabel5.setText("Time limit (sec): ");
		JButton exploreButton = new JButton("Explore");
		exploreButton.addMouseListener(new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
            	new Exploration().execute();
            }
        });
		
		exploreInputFrame.add(exploreLabel);
		exploreInputFrame.add(exploreEmpty);
		exploreInputFrame.add(exploreLabel1);
		exploreInputFrame.add(exploreLabel2);
		exploreInputFrame.add(exploreLabel3);
		exploreInputFrame.add(exploreTextField);
		exploreInputFrame.add(exploreLabel4);
		exploreInputFrame.add(exploreTextField1);	
		exploreInputFrame.add(exploreLabel5);
		exploreInputFrame.add(exploreTextField2);
		exploreInputFrame.add(exploreButton);
		exploreFrame.add(exploreInputFrame);
		
		exploreAndPath.add(exploreFrame);
		
		JPanel pathFrame = new JPanel();
		pathFrame.setBorder(new EmptyBorder(20, 20, 20, 20));
		pathFrame.setBorder(BorderFactory.createLineBorder(Color.GREEN));
		
		JPanel pathInputFrame = new JPanel(new GridLayout(4, 2));
		JTextField pathTextField = new JTextField(10);	
		JTextField pathTextField1 = new JTextField(10);
		JLabel pathLabel = new JLabel();
		pathLabel.setText("Find fastest path");
		JLabel pathEmpty = new JLabel();
		JLabel pathLabel1 = new JLabel();
		pathLabel1.setText("Speed (steps/sec): ");
		JLabel pathLabel2 = new JLabel();
		pathLabel2.setText("Time limit (sec): ");
		JButton pathButton = new JButton("Compute");
		pathButton.setEnabled(false);
		pathButton.setActionCommand("FindFastestPath");
		
		pathInputFrame.add(pathLabel);
		pathInputFrame.add(pathEmpty);
		pathInputFrame.add(pathLabel1);
		pathInputFrame.add(pathTextField);
		pathInputFrame.add(pathLabel2);
		pathInputFrame.add(pathTextField1);
		pathInputFrame.add(pathButton);
		
		pathFrame.add(pathInputFrame);
		
		exploreAndPath.add(pathFrame);
		
        overallFrame.add(mapFrame,BorderLayout.CENTER);
        overallFrame.add(exploreAndPath, BorderLayout.EAST);
        overallFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        overallFrame.pack();
        overallFrame.setVisible(true);

    }

    class Exploration extends SwingWorker<Integer, String> {
        protected Integer doInBackground() throws Exception {
        	
        	int coverage = Integer.parseInt(exploreTextField1.getText());
        	int timeLimit = Integer.parseInt(exploreTextField2.getText());

            int row, col;
            int[] robotPosition = new int[2];
            robotPosition = bot.getRobotPosition();
            row = robotPosition[0];
            col = robotPosition[1];

            bot.setRobotPos(row, col);
            rg.repaint();

            MazeExploration exploration;
            exploration = new MazeExploration(rg, bot, coverage, timeLimit);

            exploration.runExploration();
//            generateMapDescriptor(exploredMap);

            return 111;
        }
    }

}
