package algorithms;

import dataTypes.Movement;
import dataTypes.Orientation;
import simulator.Arena;
import simulator.mapGrids;
import simulatorRobot.Robot;
import simulatorRobot.RobotGenerator;

public class MazeExploration {
	
	private int[] robotPosition = new int[2];
	private Orientation robotOrientation;
	private Robot robot;
	private RobotGenerator rg, realRG;
	private long startTime;
	private long endTime;
    private final int timeLimit;
    private final int coverageLimit;
    private int areaExplored;
    
	public MazeExploration(RobotGenerator rg, Robot robot, int coverageLimit, int timeLimit) {
		this.rg = rg;
		this.robot = robot;
		this.timeLimit = timeLimit;
		this.coverageLimit = coverageLimit;
	}
	
	public int[] getRobotPosition() {
		return robotPosition;
	}
	
	public Orientation getRobotOrientation() {
		return robotOrientation;
	}
	
	public void runExploration() {

        System.out.println("Starting exploration...");

        startTime = System.currentTimeMillis();
        senseAndRepaint();

        areaExplored = calculateAreaExplored();
        System.out.println("Explored Area: " + areaExplored);
        
        int row, col;
        int[] robotPosition = new int[2];
        robotPosition = robot.getRobotPosition();
        row = robotPosition[0];
        col = robotPosition[1];
        explorationLoop(row, col);
    }
	
	private void explorationLoop(int r, int c) {
		int row, col;
        int[] robotPosition = new int[2];
        do {
        	robotPosition = robot.getRobotPosition();
            row = robotPosition[0];
            col = robotPosition[1];
            nextMove();

            areaExplored = calculateAreaExplored();
            System.out.println("Area explored: " + areaExplored);

            if (row == r && col == c) {
                if (areaExplored >= 100) {
                    break;
                }
            }
        } while (areaExplored <= coverageLimit && System.currentTimeMillis() <= endTime);

        goHome();
    }
	
	private void nextMove() {
        if (lookRight()) {
            moveBot(Movement.RIGHT);
            if (lookForward()) moveBot(Movement.FORWARD);
        } else if (lookForward()) {
            moveBot(Movement.FORWARD);
        } else if (lookLeft()) {
            moveBot(Movement.LEFT);
            if (lookForward()) moveBot(Movement.FORWARD);
        } else {
            moveBot(Movement.RIGHT);
            moveBot(Movement.RIGHT);
        }
    }
	
	/**
     * Returns true if the right side of the robot is free to move into.
     */
    private boolean lookRight() {
        switch (robot.getRobotOrientation()) {
            case NORTH:
                return eastFree();
            case EAST:
                return southFree();
            case SOUTH:
                return westFree();
            case WEST:
                return northFree();
        }
        return false;
    }

    /**
     * Returns true if the robot is free to move forward.
     */
    private boolean lookForward() {
        switch (robot.getRobotOrientation()) {
            case NORTH:
                return northFree();
            case EAST:
                return eastFree();
            case SOUTH:
                return southFree();
            case WEST:
                return westFree();
        }
        return false;
    }

    /**
     * * Returns true if the left side of the robot is free to move into.
     */
    private boolean lookLeft() {
        switch (robot.getRobotOrientation()) {
            case NORTH:
                return westFree();
            case EAST:
                return northFree();
            case SOUTH:
                return eastFree();
            case WEST:
                return southFree();
        }
        return false;
    }
    
    /**
     * Returns true if the robot can move to the north cell.
     */
    private boolean northFree() {
        int[] robotPosition = new int[2];
        robotPosition = robot.getRobotPosition();
        int botRow = robotPosition[0];
        int botCol = robotPosition[1];
        return (isExploredNotObstacle(botRow + 1, botCol - 1) && isExploredAndFree(botRow + 1, botCol) && isExploredNotObstacle(botRow + 1, botCol + 1));
    }

    /**
     * Returns true if the robot can move to the east cell.
     */
    private boolean eastFree() {
    	int[] robotPosition = new int[2];
        robotPosition = robot.getRobotPosition();
        int botRow = robotPosition[0];
        int botCol = robotPosition[1];
        return (isExploredNotObstacle(botRow - 1, botCol + 1) && isExploredAndFree(botRow, botCol + 1) && isExploredNotObstacle(botRow + 1, botCol + 1));
    }

    /**
     * Returns true if the robot can move to the south cell.
     */
    private boolean southFree() {
    	int[] robotPosition = new int[2];
        robotPosition = robot.getRobotPosition();
        int botRow = robotPosition[0];
        int botCol = robotPosition[1];
        return (isExploredNotObstacle(botRow - 1, botCol - 1) && isExploredAndFree(botRow - 1, botCol) && isExploredNotObstacle(botRow - 1, botCol + 1));
    }

    /**
     * Returns true if the robot can move to the west cell.
     */
    private boolean westFree() {
    	int[] robotPosition = new int[2];
        robotPosition = robot.getRobotPosition();
        int botRow = robotPosition[0];
        int botCol = robotPosition[1];
        return (isExploredNotObstacle(botRow - 1, botCol - 1) && isExploredAndFree(botRow, botCol - 1) && isExploredNotObstacle(botRow + 1, botCol - 1));
    }

    /**
     * Returns true for cells that are explored and not obstacles.
     */
    private boolean isExploredNotObstacle(int r, int c) {
        if (rg.checkValidCoordinates(r, c)) {
            mapGrids tmp = rg.getCell(r, c);
            return (tmp.getIsExplored() && !tmp.getIsObstacle());
        }
        return false;
    }

    /**
     * Returns true for cells that are explored, not virtual walls and not obstacles.
     */
    private boolean isExploredAndFree(int r, int c) {
        if (rg.checkValidCoordinates(r, c)) {
            mapGrids b = rg.getCell(r, c);
            return (b.getIsExplored() && !b.getIsVirtualWall() && !b.getIsObstacle());
        }
        return false;
    }

    /**
     * Returns the number of cells explored in the grid.
     */
    private int calculateAreaExplored() {
        int result = 0;
        for (int r = 0; r < Arena.MAP_HEIGHT; r++) {
            for (int c = 0; c < Arena.MAP_LENGTH; c++) {
                if (rg.getCell(r, c).getIsExplored()) {
                    result++;
                }
            }
        }
        return result;
    }
    
    private void moveBot(Movement m) {
        robot.move(m);
        rg.repaint();
        if (m != Movement.CALIBRATE) {
            senseAndRepaint();
        } else {
//            CommMgr commMgr = CommMgr.getCommMgr();
//            commMgr.recvMsg();
        }

//        if (bot.getRealBot() && !calibrationMode) {
//            calibrationMode = true;
//
//            if (canCalibrateOnTheSpot(bot.getRobotCurDir())) {
//                lastCalibrate = 0;
//                moveBot(MOVEMENT.CALIBRATE);
//            } else {
//                lastCalibrate++;
//                if (lastCalibrate >= 5) {
//                    DIRECTION targetDir = getCalibrationDirection();
//                    if (targetDir != null) {
//                        lastCalibrate = 0;
//                        calibrateBot(targetDir);
//                    }
//                }
//            }
//
//            calibrationMode = false;
//        }
    }
    
    /**
     * Returns the robot to START after exploration and points the bot northwards.
     */
    private void goHome() {
//        if (!bot.getTouchedGoal() && coverageLimit == 300 && timeLimit == 3600) {
//            FastestPathAlgo goToGoal = new FastestPathAlgo(exploredMap, bot, realMap);
//            goToGoal.runFastestPath(RobotConstants.GOAL_ROW, RobotConstants.GOAL_COL);
//        }
//
//        FastestPathAlgo returnToStart = new FastestPathAlgo(exploredMap, bot, realMap);
//        returnToStart.runFastestPath(RobotConstants.START_ROW, RobotConstants.START_COL);
//
//        System.out.println("Exploration complete!");
//        areaExplored = calculateAreaExplored();
//        System.out.printf("%.2f%% Coverage", (areaExplored / 300.0) * 100.0);
//        System.out.println(", " + areaExplored + " Cells");
//        System.out.println((System.currentTimeMillis() - startTime) / 1000 + " Seconds");
//
//        if (bot.getRealBot()) {
//            turnBotDirection(DIRECTION.WEST);
//            moveBot(MOVEMENT.CALIBRATE);
//            turnBotDirection(DIRECTION.SOUTH);
//            moveBot(MOVEMENT.CALIBRATE);
//            turnBotDirection(DIRECTION.WEST);
//            moveBot(MOVEMENT.CALIBRATE);
//        }
//        turnBotDirection(DIRECTION.NORTH);
    }
    
	private void senseAndRepaint() {
        robot.setSensors();
        robot.sense(rg, realRG);
        rg.repaint();
    }
}
