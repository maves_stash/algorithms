package simulatorRobot;

import dataTypes.Orientation;
import dataTypes.Movement;
import dataTypes.Orientation;

public class Robot {
	private static Robot robot;
	private int speed;
	private int[] robotPosition = new int[2];
//	private int robotPosX, robotPosY;
	private Orientation robotOrientation;
	private final Sensor SRFrontLeft;       // north-facing front-left SR
    private final Sensor SRFrontCenter;     // north-facing front-center SR
    private final Sensor SRFrontRight;      // north-facing front-right SR
    private final Sensor SRLeft;            // west-facing left SR
    private final Sensor SRRight;           // east-facing right SR
    private final Sensor LRLeft; 
    
	private static final int ROW_START = 1;
	private static final int COL_START = 1;

	public static final int SENSOR_SHORT_RANGE_L = 1;               // range of short range sensor (cells)
    public static final int SENSOR_SHORT_RANGE_H = 2;               // range of short range sensor (cells)
    public static final int SENSOR_LONG_RANGE_L = 3;                // range of long range sensor (cells)
    public static final int SENSOR_LONG_RANGE_H = 4;                // range of long range sensor (cells)
    
	public Robot() {
//		this.robotPosX = ROW_START;
//		this.robotPosY = COL_START;
		robotPosition[0] = 1;
		robotPosition[1] = 1;
		robotOrientation = Orientation.NORTH;
		
		SRFrontLeft = new Sensor(SENSOR_SHORT_RANGE_L, SENSOR_SHORT_RANGE_H, this.robotPosition[1] + 1, this.robotPosition[0] - 1, this.robotOrientation, "SRFL");
        SRFrontCenter = new Sensor(SENSOR_SHORT_RANGE_L, SENSOR_SHORT_RANGE_H, this.robotPosition[1] + 1, this.robotPosition[0], this.robotOrientation, "SRFC");
        SRFrontRight = new Sensor(SENSOR_SHORT_RANGE_L, SENSOR_SHORT_RANGE_H, this.robotPosition[1] + 1, this.robotPosition[0] + 1, this.robotOrientation, "SRFR");
        SRLeft = new Sensor(SENSOR_SHORT_RANGE_L, SENSOR_SHORT_RANGE_H, this.robotPosition[1] + 1, this.robotPosition[0] - 1, findNewDirection(Movement.LEFT), "SRL");
        SRRight = new Sensor(SENSOR_SHORT_RANGE_L, SENSOR_SHORT_RANGE_H, this.robotPosition[1] + 1, this.robotPosition[0] + 1, findNewDirection(Movement.RIGHT), "SRR");
        LRLeft = new Sensor(SENSOR_LONG_RANGE_L, SENSOR_LONG_RANGE_H, this.robotPosition[1], this.robotPosition[0] - 1, findNewDirection(Movement.LEFT), "LRL");
    }

	public int[] getRobotPosition(){
		return robotPosition;
	}
	
	/**
     * Overloaded method that calls this.move(MOVEMENT m, boolean sendMoveToAndroid = true).
     */
    public void move(Movement m) {
        this.move(m, true);
    }
	
    /**
     * Takes in a MOVEMENT and moves the robot accordingly by changing its position and direction. Sends the movement
     * if this.realBot is set.
     */
    public void move(Movement m, boolean sendMoveToAndroid) {
//        if (!realBot) {
//            // Emulate real movement by pausing execution.
//            try {
//                TimeUnit.MILLISECONDS.sleep(speed);
//            } catch (InterruptedException e) {
//                System.out.println("Something went wrong in Robot.move()!");
//            }
//        }

        switch (m) {
            case FORWARD:
                switch (robotOrientation) {
                    case NORTH:
                        robotPosition[1]++;
                        break;
                    case EAST:
                    	robotPosition[0]++;
                        break;
                    case SOUTH:
                    	robotPosition[1]--;
                        break;
                    case WEST:
                    	robotPosition[0]--;
                        break;
                }
                break;
            case BACKWARD:
                switch (robotOrientation) {
                    case NORTH:
                    	robotPosition[1]--;
                        break;
                    case EAST:
                    	robotPosition[0]--;
                        break;
                    case SOUTH:
                    	robotPosition[1]++;
                        break;
                    case WEST:
                    	robotPosition[0]++;
                        break;
                }
                break;
            case RIGHT:
            case LEFT:
            	robotOrientation = findNewDirection(m);
                break;
            case CALIBRATE:
                break;
            default:
                System.out.println("Error in Robot.move()!");
                break;
        }

//        if (realBot) sendMovement(m, sendMoveToAndroid);
//        else System.out.println("Move: " + MOVEMENT.print(m));
//
//        updateTouchedGoal();
    }
    
	public Orientation getRobotOrientation() {
		return robotOrientation;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	
	public void setRobotPos(int row, int col) {
		robotPosition[1] = row;
        robotPosition[0] = col;
    }
	
	private Orientation findNewDirection(Movement m) {
        if (m == Movement.RIGHT) {
            return Orientation.getNext(robotOrientation);
        } else {
            return Orientation.getPrevious(robotOrientation);
        }
    }
	
	public void setSensors() {
        switch (robotOrientation) {
            case NORTH:
                SRFrontLeft.setSensor(this.robotPosition[1] + 1, this.robotPosition[0] - 1, this.robotOrientation);
                SRFrontCenter.setSensor(this.robotPosition[1] + 1, this.robotPosition[0], this.robotOrientation);
                SRFrontRight.setSensor(this.robotPosition[1] + 1, this.robotPosition[0] + 1, this.robotOrientation);
                SRLeft.setSensor(this.robotPosition[1] + 1, this.robotPosition[0] - 1, findNewDirection(Movement.LEFT));
                LRLeft.setSensor(this.robotPosition[1], this.robotPosition[0] - 1, findNewDirection(Movement.LEFT));
                SRRight.setSensor(this.robotPosition[1] + 1, this.robotPosition[0] + 1, findNewDirection(Movement.RIGHT));
                break;
            case EAST:
                SRFrontLeft.setSensor(this.robotPosition[1] + 1, this.robotPosition[0] + 1, this.robotOrientation);
                SRFrontCenter.setSensor(this.robotPosition[1], this.robotPosition[0] + 1, this.robotOrientation);
                SRFrontRight.setSensor(this.robotPosition[1] - 1, this.robotPosition[0] + 1, this.robotOrientation);
                SRLeft.setSensor(this.robotPosition[1] + 1, this.robotPosition[0] + 1, findNewDirection(Movement.LEFT));
                LRLeft.setSensor(this.robotPosition[1] + 1, this.robotPosition[0], findNewDirection(Movement.LEFT));
                SRRight.setSensor(this.robotPosition[1] - 1, this.robotPosition[0] + 1, findNewDirection(Movement.RIGHT));
                break;
            case SOUTH:
                SRFrontLeft.setSensor(this.robotPosition[1] - 1, this.robotPosition[0] + 1, this.robotOrientation);
                SRFrontCenter.setSensor(this.robotPosition[1] - 1, this.robotPosition[0], this.robotOrientation);
                SRFrontRight.setSensor(this.robotPosition[1] - 1, this.robotPosition[0] - 1, this.robotOrientation);
                SRLeft.setSensor(this.robotPosition[1] - 1, this.robotPosition[0] + 1, findNewDirection(Movement.LEFT));
                LRLeft.setSensor(this.robotPosition[1], this.robotPosition[0] + 1, findNewDirection(Movement.LEFT));
                SRRight.setSensor(this.robotPosition[1] - 1, this.robotPosition[0] - 1, findNewDirection(Movement.RIGHT));
                break;
            case WEST:
                SRFrontLeft.setSensor(this.robotPosition[1] - 1, this.robotPosition[0] - 1, this.robotOrientation);
                SRFrontCenter.setSensor(this.robotPosition[1], this.robotPosition[0] - 1, this.robotOrientation);
                SRFrontRight.setSensor(this.robotPosition[1] + 1, this.robotPosition[0] - 1, this.robotOrientation);
                SRLeft.setSensor(this.robotPosition[1] - 1, this.robotPosition[0] - 1, findNewDirection(Movement.LEFT));
                LRLeft.setSensor(this.robotPosition[1] - 1, this.robotPosition[0], findNewDirection(Movement.LEFT));
                SRRight.setSensor(this.robotPosition[1] + 1, this.robotPosition[0] - 1, findNewDirection(Movement.RIGHT));
                break;
        }

    }
	
	public int[] sense(RobotGenerator explorationMap, RobotGenerator realMap) {
        int[] result = new int[6];

//        if (!realBot) {
//            result[0] = SRFrontLeft.sense(explorationMap, realMap);
//            result[1] = SRFrontCenter.sense(explorationMap, realMap);
//            result[2] = SRFrontRight.sense(explorationMap, realMap);
//            result[3] = SRLeft.sense(explorationMap, realMap);
//            result[4] = SRRight.sense(explorationMap, realMap);
//            result[5] = LRLeft.sense(explorationMap, realMap);
//        } else {
//            CommMgr comm = CommMgr.getCommMgr();
//            String msg = comm.recvMsg();
//            String[] msgArr = msg.split(";");
//
//            if (msgArr[0].equals(CommMgr.SENSOR_DATA)) {
//                result[0] = Integer.parseInt(msgArr[1].split("_")[1]);
//                result[1] = Integer.parseInt(msgArr[2].split("_")[1]);
//                result[2] = Integer.parseInt(msgArr[3].split("_")[1]);
//                result[3] = Integer.parseInt(msgArr[4].split("_")[1]);
//                result[4] = Integer.parseInt(msgArr[5].split("_")[1]);
//                result[5] = Integer.parseInt(msgArr[6].split("_")[1]);
//            }

            SRFrontLeft.senseReal(explorationMap, result[0]);
            SRFrontCenter.senseReal(explorationMap, result[1]);
            SRFrontRight.senseReal(explorationMap, result[2]);
            SRLeft.senseReal(explorationMap, result[3]);
            SRRight.senseReal(explorationMap, result[4]);
            LRLeft.senseReal(explorationMap, result[5]);

//            String[] mapStrings = MapDescriptor.generateMapDescriptor(explorationMap);
//            comm.sendMsg(mapStrings[0] + " " + mapStrings[1], CommMgr.MAP_STRINGS);
//        }

        return result;
    }
}
