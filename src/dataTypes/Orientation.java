package dataTypes;

public enum Orientation {
	NORTH, EAST, SOUTH, WEST;
	
	public static Orientation getNext(Orientation curOrientation) {
        return values()[(curOrientation.ordinal() + 1) % values().length]; // Return the position in its enum declaration
    }

    public static Orientation getPrevious(Orientation curOrientation) {
        return values()[(curOrientation.ordinal() + values().length - 1) % values().length];
    }

    public static char print(Orientation o) {
        switch (o) {
            case NORTH:
                return 'N';
            case EAST:
                return 'E';
            case SOUTH:
                return 'S';
            case WEST:
                return 'W';
            default:
                return 'X';
        }
    }
}
