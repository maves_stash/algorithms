package dataTypes;

public enum Movement {
	FORWARD, BACKWARD, RIGHT, LEFT, CALIBRATE, ERROR;
}
