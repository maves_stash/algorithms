package tcpconn;

import java.io.BufferedWriter;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class ClientMgr {

    public static final String RPI_IP_ADDRESS = "192.168.2.2";
    public static final int RPI_PORT = 3053;

    private static ClientMgr _instance = null;
    private Socket _conn = null;

    public static ClientMgr getInstance(){
        if (_instance == null){
            _instance = new ClientMgr();
        }
        return _instance;
    }


    public void setUpConnection(){
        System.out.println("Connecting...");
        try{
            _conn = new Socket(RPI_IP_ADDRESS, RPI_PORT);

            System.out.println("Connection established");
        }catch (UnknownHostException e){
            System.out.println("UnknownHostException");
        }catch (IOException e){
            System.out.println("IOException");
        }catch (Exception e){
            System.out.println("Exception");
            e.printStackTrace();
        }
    }

    public void closeconnection(){
        System.out.println("Connection closing...");
        try{
            if (!_conn.isClosed()){
                _conn.close();
            }
        }catch (IOException e){
            System.out.println("IOException e");
        }catch (Exception e){
            System.out.println("Exception");
            e.printStackTrace();
        }

    }

//    public void sendMessage(){}
//    public void readMessage(){}
}
